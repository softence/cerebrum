<?php

namespace Cerebrum\Command;

use Cerebrum\Doctrine\DatabaseSchemaInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package Cerebrum
 */
class SchemaCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var DatabaseSchemaInterface
     */
    protected $schema;

    /**
     * @param DatabaseSchemaInterface $schema
     */
    public function __construct(DatabaseSchemaInterface $schema)
    {
        $this->schema = $schema;
    }
    
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('schema')
            ->setDescription('Update database schema')
            ->addOption('force', null, InputOption::VALUE_NONE, 'If set the queries will be executed.')
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pimple = $this->getApplication()->getPimple();

        $output->writeln('<info>Updating database schema...:</info>');

        $force   = $input->getOption('force');
        $schema  = $pimple['db']->getSchemaManager()->createSchema();
        $queries = $schema->getMigrateToSql($this->schema->createSchema(), $pimple['db']->getDatabasePlatform());

        foreach ($queries as $query) {
            $output->writeln('    <comment>' . ($force ? 'Executed' : 'Dry Run') . ':</comment> ' .  $query);

            if ($force) {
                $pimple['db']->executeQuery($query);
            }
        }

        if (!$queries) {
            $output->writeln('    <comment>The database is up-to-date.</comment>');
        }
    }
}
