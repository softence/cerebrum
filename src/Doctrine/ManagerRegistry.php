<?php

namespace Cerebrum\Doctrine;

use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Silex\Application;

/**
 * Glue class between silex and doctrine. It is required
 * when using DoctrineOrmExtension in symfony forms.
 *
 * More information:
 *    https://github.com/symfony/DoctrineBridge
 *    https://github.com/umpirsky/silex-on-steroids
 *    http://symfony.com/doc/current/reference/forms/types/entity.html
 *
 * @package Cerebrum
 */
class ManagerRegistry extends AbstractManagerRegistry
{
    /**
     * @var Application
     */
    protected $container;

    /**
     * @param Application $container
     */
    public function setContainer(Application $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    protected function getService($name)
    {
        return $this->container[$name];
    }

    /**
     * {@inheritdoc}
     */
    protected function resetService($name)
    {
        throw new \BadMethodCallException;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliasNamespace($alias)
    {
        return $this->container['orm.config']->getEntityNamespace($alias);
    }
}
