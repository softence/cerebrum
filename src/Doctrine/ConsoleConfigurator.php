<?php

namespace Cerebrum\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;

final class ConsoleConfigurator
{
    public static function configure(Application $console, EntityManagerInterface $manager)
    {
        self::mergeHelperSet($console->getHelperSet(), ConsoleRunner::createHelperSet($manager));

        // Add doctrine commands to the console
        ConsoleRunner::addCommands($console);

        return $console;
    }

    private static function mergeHelperSet(HelperSet $original, HelperSet $merger)
    {
        foreach ($merger as $alias => $helper) {
            $original->set($helper, is_integer($alias) ? null : $alias);
        }
    }
}
