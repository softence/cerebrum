<?php

namespace Cerebrum\Provider;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver;
use Doctrine\ORM\Tools\Setup;
use Cerebrum\Doctrine\ManagerRegistry;
use Cerebrum\Doctrine\ConsoleConfigurator;
use Silex\Application;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;

/**
 * @package Cerebrum
 */
class DoctrineOrmServiceProvider implements \Silex\ServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function register(Application $app)
    {
        $app['orm.cache']     = new ArrayCache;
        $app['orm.options']   = [];
        $app['orm.proxy_dir'] = null;
        $app['orm.mapping']   = [];

        $app['orm.config'] = $app->share(function ($app) {
            $config = Setup::createConfiguration($app['debug'], $app['orm.proxy_dir'], $app['orm.cache']);
            $config->setMetadataDriverImpl(new SimplifiedXmlDriver(array_flip($app['orm.mapping'])));

            return $config;
        });

        $app['orm.em'] = $app->share(function ($app) {
            return EntityManager::create($app['orm.options'], $app['orm.config']);
        });

        $app['orm.registry'] = $app->share(function($app) {
            $registry = new ManagerRegistry(null, [], ['orm.em'], null, null, 'Doctrine\ORM\Proxy\Proxy');
            $registry->setContainer($app);

            return $registry;
        });

        if (isset($app['console'])) {
            $app['console'] = $app->share($app->extend('console', function ($console, $app) {
                return ConsoleConfigurator::configure($console, $app['orm.em']);
            }));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function boot(Application $app)
    {
    }
}
