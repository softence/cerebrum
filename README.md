Cerebrum
========

Adds [Doctrine ORM](http://www.doctrine-project.org/projects/orm.html) support to Silex without any hard depencies.

Installation
------------
Install this project through Composer by adding the following to your `composer.json` file:

```JSON
    "require" : {
        "doctrine/orm"           : "~2.5",
        "softence/cerebrum"      : "~0.1"
    },

    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:softence/cerebrum.git"
        }
    ]
```

Configuration
-------------
Register the `Cerebrum\Provider\DoctrineOrmServiceProvider` in your application, i.e.:

```PHP
$app->register(new Cerebrum\Provider\DoctrineOrmServiceProvider);
```

Add the following to the configuration of your project and makes changes accordingly:

```JSON
"orm.options": {
    "driver"   : "pdo_mysql",
    "user"     : "root",
    "password" : "123",
    "dbname"   : "skeleton",
    "host"     : "localhost",
    "port"     : 3306
},

"orm.mapping": {
    "Softence\\Sekeleton\\Entity": "%root_dir%/src/Resources/doctrine"
}
```

### Namespace Aliasing

Enable namespace aliasing eg. `"MyNamespace:User" -> "MyNamespace\Entity\User"`:

```PHP
<?php
$app['orm.config'] = $app->extend('orm.config', function($config) {
    $config->addEntityNamespace('MyNamespace', 'MyNamespace\\Entity');

    return $config;
});
```

### Forms

Using doctrine entities in forms requires `symfony/form` and `symfony/doctrine-bridge`.

Enable the form extension:

```PHP
<?php

$app['form.extensions'] = $app->share($app->extend('form.extensions', function ($extensions) use ($app) {
    $extensions[] = new \Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension($app['orm.registry']);

    return $extensions;
}));
```

Usage with the form builder:

```PHP
<?php

$form = $app['form.factory']->createBuilder('form', $data)
    ->add('user', 'entity', [
        'class' => 'MyNamespace\\Entity\\User', // or MyNamespace::User if using namespace aliasing
        // ... more options
    ])
;
```

For a complete list of options to the entity type see
[the Symfony documentation](http://symfony.com/doc/current/reference/forms/types/entity.html).

### Validation

Using the unqiue entity contraint for validation requires `symfony/validator` and `symfony/doctrine-bridge`.

Setup the validator:

```PHP
<?php

$app['orm.validator.unique'] = $app->share(function ($app) {
    return new \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator($app['orm.registry']);
});

$app['validator.validator_service_ids'] = [
    'doctrine.orm.validator.unique' => 'orm.validator.unique',
];
```

Example, entity validation:

```php
<?php

namespace MyNamespace\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class User
{
    protected $email;

    public static function loadValidatorMetadata(\Symfony\Component\Validator\Mapping\ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['email'],
        ]));
    }
}
```
