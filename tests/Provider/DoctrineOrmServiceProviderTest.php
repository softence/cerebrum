<?php

namespace Flex\TwigBootstrap\Test\Provider;

use Doctrine\ORM\EntityManager;
use Cerebrum\Doctrine\ManagerRegistry;
use Cerebrum\Provider\DoctrineOrmServiceProvider;
use Silex\Application;
use Symfony\Component\Console\Application as Console;

class DoctrineOrmServiceProviderTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $app = new Application;
        $app['debug'] = true;
        $app->register(new DoctrineOrmServiceProvider, [
            'orm.proxy_dir' => 'test',
            'orm.options' => [
                'driver' => 'pdo_sqlite',
                'memory' => true,
            ],
            'orm.mapping' => [
                'Namespace1' => 'location1',
                'Namespace2' => 'location2',
            ],
        ]);

        $app['orm.config'] = $app->extend('orm.config', function($config) {
            $config->addEntityNamespace('Namespace1', 'Namespace1\\Entity');
            $config->addEntityNamespace('Namespace2', 'Namespace2\\Entity');

            return $config;
        });

        $this->app = $app;
    }

    public function testOrmIsConfigured()
    {
        $config = $this->app['orm.config'];

        $this->assertEquals($config->getProxyDir(), 'test');
        $this->assertEquals($config->getMetadataDriverImpl()->getLocator()->getNamespacePrefixes(), [
            'location1' => 'Namespace1',
            'location2' => 'Namespace2',
        ]);
        $this->assertEquals($config->getEntityNamespaces(), [
            'Namespace1' => 'Namespace1\\Entity',
            'Namespace2' => 'Namespace2\\Entity',
        ]);
    }

    public function testEntityManagerExists()
    {
        $this->assertTrue($this->app['orm.em'] instanceof EntityManager);
    }

    public function testManagerRegistryExists()
    {
        $this->assertTrue($this->app['orm.registry'] instanceof ManagerRegistry);
    }


    public function testConsoleIsConfigured()
    {
        $app = new Application;
        $app['console'] = $app->share(function () {
            return new Console('console');
        });

        $app->register(new DoctrineOrmServiceProvider, [
            'orm.options' => [
                'driver' => 'pdo_sqlite',
                'memory' => true,
            ],
        ]);

        $console = $app['console'];
        $helperSet = $console->getHelperSet();

        $this->assertTrue($console->has('orm:schema-tool:update'));
        $this->assertTrue($helperSet->has('em'));
        $this->assertTrue($helperSet->has('db'));
    }
}
